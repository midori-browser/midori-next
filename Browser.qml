import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtWebEngine 1.9
import QtQuick.Window 2.10

ApplicationWindow {
    id: window
    property ListModel tabs: ListModel {
        ListElement {
            icon: ""
            url: "http://example.com"
            title: "Example"
            audio: false
        }
        ListElement {
            icon: ""
            url: "https://www.midori-browser.org"
            title: "Midori Web Browser"
            audio: false
        }
        ListElement {
            icon: ""
            url: "https://qt.io"
            title: "Qt Developer Website"
            audio: false
        }
    }

    visible: true
    minimumWidth: 100
    minimumHeight: 250
    width: Screen.width / 2
    height: Screen.height / 2
    flags: titleBar.csd ? Qt.Window | Qt.CustomizeWindowHint : Qt.Window
    title: tabs.get(stack.currentIndex).title

    Component.onCompleted: {
        Qt.application.displayName = qsTr('Midori Web Browser')
    }

    header: TitleBar {
        id: titleBar
        visible: window.visibility !== Window.FullScreen
        TabBar {
            id: tabBar
            Layout.fillWidth: true
            // Drag space
            Layout.margins: titleBar.showDecoration ? 8 : 0
            Layout.bottomMargin: 0
            position: titleBar.csd ? TabBar.Footer : TabBar.Header

            Repeater {
                model: tabs

                TabButton {
                    topPadding: titleBar.showDecoration ? 0 : padding
                    rightPadding: 0

                    background: Rectangle {
                        visible: !checked
                        color: header.background.color
                        opacity: hovered ? 0.5 : 1.0
                        Behavior on opacity { PropertyAnimation {} }
                    }
                    Action {
                        shortcut: "Ctrl+W"
                        enabled: stack.currentIndex === model.index
                        onTriggered: tabs.remove(stack.currentIndex)
                    }
                    contentItem: RowLayout {
                        ToolButton {
                            icon.name: "audio-volume-high-symbolic"
                            icon.width: 16
                            icon.height: 16
                            text: icon.height ? '' : "🔊"
                            visible: audio
                            enabled: false
                            background: null
                        }
                        Image {
                            source: model.icon
                            sourceSize.width: 16
                            sourceSize.height: 16
                        }
                        Label {
                            id: title
                            text: model.title
                            elide: Text.ElideRight
                            Layout.fillWidth: true
                        }
                        ToolButton {
                            icon.name: "window-close-symbolic"
                            icon.width: 16
                            icon.height: 16
                            background.implicitWidth: padding
                            background.implicitHeight: padding
                            text: icon.height ? '' : "x"
                            onClicked: tabs.remove(stack.currentIndex)
                        }
                    }
                }
            }
        }

        MenuToolButton {
            icon.name: "tab-new-symbolic"
            text: icon.height ? '' : "+"
            action: Action {
                shortcut: "Ctrl+T"
                onTriggered: tabs.append({url:"",title:"about:blank"})
            }
        }
        MenuToolButton {
            icon.name: "view-fullscreen-symbolic"
            text: icon.height ? '' : "⇱︎"
            onClicked: window.visibility === Window.FullScreen ? window.showNormal() : window.showFullScreen()
            action: Action {
                shortcut: "F11"
                onTriggered: window.visibility == Window.FullScreen ? window.showNormal() : window.showFullScreen()
            }
        }
        MenuToolButton {
            icon.name: "open-menu-symbolic"
            text: icon.height ? '' : "⋮"

            acceptedButtons: Qt.LeftButton
            menu: Menu {
                Action { text: "&Clear private data"; shortcut: "Ctrl+Shift+Delete"; onTriggered: clearPrivateData.open(); }
                Action { text: "Close a&l Windows"; shortcut: "Ctrl+Q"; onTriggered: Qt.quit(); }
            }
        }
    }

    StackLayout {
        id: stack
        currentIndex: tabBar.currentIndex
        anchors.fill: parent

        Repeater {
            anchors.fill: parent
            model: tabs

            GridLayout {
                flow: GridLayout.TopToBottom
                ToolBar {

                    background: Rectangle {
                        color: window.color
                    }
                    Layout.fillWidth: true
                    RowLayout {
                        anchors.fill: parent

                        MenuToolButton {
                            icon.name: "go-previous-symbolic"
                            text: icon.height ? '' : "<"
                            onClicked: webView.goBack()
                            enabled: webView.canGoBack

                            menu: Menu {
                                Repeater {
                                    model: webView.navigationHistory.backItems
                                    MenuItem { text: model.title; onTriggered: webView.goBackOrForward(model.offset); }
                                }
                            }
                        }
                        MenuToolButton {
                            icon.name: "go-next-symbolic"
                            text: icon.height ? '' : ">"
                            onClicked: webView.goForward()
                            enabled: webView.canGoForward
                            menu: Menu {
                                Repeater {
                                    model: webView.navigationHistory.forwardItems
                                    MenuItem { text: model.title; onTriggered: webView.goBackOrForward(model.offset); }
                                }
                            }
                        }
                        MenuToolButton {
                            icon.name: "view-refresh-symbolic"
                            text: icon.height ? '' : "@"
                            onClicked: webView.reload()
                            visible: !webView.loading
                        }
                        MenuToolButton {
                            icon.name: "process-stop-symbolic"
                            text: icon.height ? '' : "x"
                            onClicked: webView.stop()
                            visible: webView.loading
                        }

                        TextField {
                            id: urlBar
                            Layout.fillWidth: true
                            placeholderText: qsTr("Search or enter an address")
                            text: model.url
                            onAccepted: model.url = text
                            selectByMouse: true
                        }
                    }
                }

                SplitView {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    orientation: Qt.Vertical

                    WebEngineView {
                        id: webView
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        SplitView.fillHeight: true
                        url: model.url
                        devToolsView: tools
                        onUrlChanged: model.url = url.toString()
                        onTitleChanged: model.title = title
                        onIconChanged: model.icon = icon.toString()
                        onRecentlyAudibleChanged: model.audio = recentlyAudible
                    }

                    WebEngineView {
                        id: tools
                        SplitView.preferredHeight: webView.inspectedView !== undefined ? 150 : 0
                    }

                }
                Action {
                    shortcut: "Ctrl+L"
                    enabled: stack.currentIndex === model.index
                    onTriggered: urlBar.forceActiveFocus()
                }
            }


        }
    }

    Drawer {
        id: panel
        y: header.height
        width: window.width * 0.3
        height: window.height - header.height
        position: 1.0
        ListView {
            anchors.fill: parent
            orientation: ListView.Vertical
            model: tabs
            delegate: TabButton {
                text: title
                onClicked: stack.currentIndex = index
            }

        }
    }

    Dialog {
        id: clearPrivateData
        title: qsTr("Clear Private Data")
        anchors.centerIn: parent
        modal: true

        Column {
            Label {
                text: qsTr("Clear the following data:")
            }

            Switch {
                id: clearWebCache
                text: qsTr("Web Cache")
            }
        }

        header: titleBar.csd ? buttons : null
        footer: titleBar.csd ? null : buttons
        property Item buttons: DialogButtonBox {
            Button {
                text: qsTr("&Clear private data")
                DialogButtonBox.buttonRole: DialogButtonBox.DestructiveRole
                onClicked: {
                    if (clearWebCache.checked)
                        WebEngine.defaultProfile.clearHttpCache()
                    clearPrivateData.close()
                }
            }
            Button {
                text: qsTr("&Cancel")
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
                onClicked: clearPrivateData.close()
            }
        }
    }
}
