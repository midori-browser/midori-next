import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Window 2.10

ToolBar {
    default property alias buttons: row.children
    property color frameColor: "#4da80d"
    property bool csd: true
    property bool showDecoration: csd && Window.visibility === Window.Windowed

    RowLayout {
        id: row
        anchors.fill: parent
        spacing: 0
        anchors.margins: 0

        MenuToolButton {
            icon.name: "window-close-symbolic"
            text: icon.height ? '' : "x"
            // Drag space
            Layout.leftMargin: 8
            onClicked: window.close()
            visible: showDecoration
        }
    }

    background: Rectangle {
        color: csd ? frameColor : Window.window.color

        MouseArea {
            anchors.fill: parent
            z: -1
            property point prev
            onPressed: {
                prev = Qt.point(mouse.x,mouse.y)
            }

            onDoubleClicked: window.visibility == Window.Maximized ? window.showNormal() : window.showMaximized()

            onPositionChanged: {
                if(window.visibility == Window.Windowed && pressed) {
                    window.showNormal();
                    var delta = Qt.point(mouse.x - prev.x, mouse.y - prev.y);
                    window.x += delta.x;
                    window.y += delta.y;
                }
            }
            enabled: csd
        }
    }
}
